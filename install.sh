#!/bin/bash

# https://github.com/mssola/dotfiles/blob/master/install.sh
PRJ="$HOME/dotfiles"

set -e

echo "set bashrc"
ln -sf $PRJ/.bashrc $HOME/
echo "set screenrc"
ln -sf $PRJ/.screenrc $HOME/
if [ -d "$DIRECTORY" ]; then
    echo "emacs.d not exist, clone from bitbucket"
    git clone https://bitbucket.org/liaoxiaorong/emacs.d.git $HOME/.emacs.d
else
    echo "emacs.d already exist\nyou can clone it from https://bitbucket.org/liaoxiaorong/emacs.d.git"
fi
